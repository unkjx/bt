import datetime
import functools
import json
import math
import os
import pathlib
import random
import subprocess as sp
import sys
import time

def seed():
    MAXINT32ABS = 2147483647
    return random.randint(-MAXINT32ABS, MAXINT32ABS)

class Task:
    def __init__(self, name, cmd, log_file, result_file, max_trials):
        self.name = name
        self.cmd = cmd
        self.log_file = log_file
        self.result_file = result_file
        self.max_trials = max_trials
        
        self.trials = 0
        self.tmp_file = "tmp_" + str(datetime.datetime.now()).replace(" ", "_")

    def start(self):
        can_start = self.trials <= self.max_trials
        if can_start:
            self.trials += 1
            self.finished = False
            self.proc = sp.Popen(self.cmd + " > " + self.tmp_file + " 2>&1", shell = True)
        return can_start

    def started(self):
        return self.trials > 0

    def completed(self):
        returned = self.started() and self.proc.poll() != None
        if returned and not self.finished:
            self.finished = True
            os.system("cat " + self.tmp_file + " >> " + self.log_file)
            os.system("echo \"" + self.name + ":\" >> " + self.result_file)
            # task failed?
            if os.system("cat " + self.tmp_file + " | grep 'Local Search Results:' > /dev/null") != 0:
                [os.system("echo " + str(self.proc.returncode) + " >> " + f)
                    for f in (self.log_file, self.result_file)]
                print("!", end = "", flush = True)
                return not self.start()
            else:
                sp.call(["bash", "-c", "cat " + self.tmp_file
                    + " | grep \"spectral results\" | cat <(echo \""
                    + self.name + ": \") - >> " + self.result_file])
                print(".", end = "", flush = True)
            os.system("rm "+ self.tmp_file)
        return returned

def write_julia_config_file(path, params):
    def stringify_param(p):
        if type(p) == str:
            return "\"" + p + "\""
        elif callable(p):
            return p()
        elif type(p) == bool:
            return str(p).lower()
        else:
            return str(p)
    jl_config_file = open(path, "w")
    jl_config_file.writelines([
        "config_" + str(k) + "=" + stringify_param(v) + "\n"
        for k, v in params.items()])
    jl_config_file.close()

def run_to_param_indices(run, param_lens):
    res = [0 for i in range(len(param_lens))]
    acc = run
    for i in range(len(param_lens)):
        res[i] = acc % param_lens[i]
        acc = math.floor(acc / param_lens[i])
    return res

def get_param(experiment, indices, name):
    ind = list(experiment["parameters"].keys()).index(name)
    return list(experiment["parameters"].values())[ind][indices[ind]]

ex_name = sys.argv[1]
stamp_str = str(datetime.datetime.now()).replace(" ", "_")
result_file_name = ex_name + "_results_" + stamp_str + ".txt"
log_file_name = ex_name + "_log_" + stamp_str + ".txt"

ex_file = open("experiments.json")
ex = json.load(ex_file)[ex_name]
ex_file.close()

if not os.path.exists(ex["tmpDir"]):
    os.mkdir(ex["tmpDir"])

instances = functools.reduce(lambda arr, i_dir : arr + [str(p) for p in pathlib.Path(i_dir).rglob("*.*")], ex["instanceFolders"], [])

param_lengths = [len(p) for p in ex["parameters"].values()]
num_runs = functools.reduce(lambda n, l : n * l, param_lengths, 1)

num_cores = ex["cores"][0] if len(ex["cores"]) == 1 else ex["cores"][1] - ex["cores"][0] + 1
taskset_cmd = "taskset -c " + str(ex["cores"][0]) + "-" + str(ex["cores"][1]) + " " if len(ex["cores"]) == 2 else ""
os.system("export JULIA_NUM_THREADS=" + str(num_cores))

os.system("echo \"" + ex_name + "\" > " + result_file_name)

print("launching experiment " + ex_name + " (" + str(num_runs) + " runs, " + str(len(instances)) + " instances, " + str(ex["numSeeds"]) +" seeds)",
    end = "", flush = True)
for i in range(num_runs):
    indices = run_to_param_indices(i, param_lengths)
    write_julia_config_file(ex["juliaConfFile"],
        dict({ p : get_param(ex, indices, p) for p in ex["parameters"].keys()},
        **{"verbose": False,
            "verbose_limits": [25, 500],
            "seed": seed()},
        **{k: ex[k] for k in ("tmpDir", "installationsPrefix")})
    )

    os.system("echo \"" + str(i + 1) + "------------\n\" >> " + result_file_name)

    os.system("cat " + ex["juliaConfFile"] + " >> " + result_file_name)

    print("\nrun " + str(i + 1), end = "", flush = True)

    task_queue = [Task(
        inst.split("/")[-1],
        (
            "timeout --kill-after=1m " + str(ex["taskTimeout"]) + "m "
            + taskset_cmd + ex["mtkahyparFile"] + " "
            + "-h\"" + inst + "\" "
            + "-t" + str(num_cores) + " "
            + "--seed=" + str(seed()) + " "
            + ("--r-spectral-type=spectral --r-spectral-config=\"" + str(get_param(ex, indices, "numCandidates")) + "\" " # TODO ;)
                if not "noSpectral" in ex else "")
            + "-k" + str(get_param(ex, indices, "k")) + " "
            + ("--instance-type=graph --input-file-format=metis " if inst.endswith(".graph") else "")
            + "-e0.03 -ocut -mdirect --preset-type=default -v1 --show-detailed-timings 1"
        ),
        log_file_name,
        result_file_name,
        3) for inst in instances for _ in range(ex["numSeeds"])]
    
    random.shuffle(task_queue)
    
    while True:
        running = 0
        for t in task_queue:
            if t.started() and not t.completed():
                running += 1
        
        for t in task_queue:
            if running == ex["numTasks"]:
                break
            if not t.started():
                t.start()
                running += 1
        
        if running == 0:
            break

        time.sleep(1)

print("\nfinished, results in " + result_file_name)
