import sys
import matplotlib.pyplot as plt
import re

BEGIN = 0
ROUND = 1
METADATA = 2
INSTANCE = 3
FIRST_RESULT = 4
RESULTS = 5


result_file_name = sys.argv[1]

f = open(result_file_name)
data = {"meta": [], "res": [], "fails":[]}
state = BEGIN
for l in f.readlines():
    line = l.strip("\n\r ")

    if len(line) == 0:
        continue

    if state == BEGIN:
        state = ROUND
        continue


    if state == METADATA:
        if line.count("=") == 0:
            state = INSTANCE
        else:
            variable = line.split("=")
            data["meta"][-1][variable[0]] = variable[1]
            continue
    
    if state == FIRST_RESULT:
        if line.count("spectral results") == 0:
            data["fails"][-1] += 1
        state = RESULTS

    if state == RESULTS:
        if line.count("spectral results") == 0:
            state = INSTANCE
        else:
            regex_res : re.Match = re.search(r"best\_cutsize \= (\d+) ,  best\_metrics\.quality \= (\-?\d+)", line)
            if not regex_res:
                print(line)
            data["res"][-1] += [float(int(regex_res.group(1))) / float(int(regex_res.group(2)))]
            continue
    
    if state == INSTANCE:
        if line.count(".graph:") == 0 and line.count(".hgr:") == 0:
            data["res"][-1].sort()
            state = ROUND
        else:
            state = FIRST_RESULT
            continue

    if state == ROUND:
        data["meta"] += [{}]
        data["res"] += [[]]
        data["fails"] += [0]
        state = METADATA
        continue

data["res"][-1].sort()


rounds = len(data["res"])

dx = lambda arr_x : (lambda k_v : arr_x[k_v[0] + 1] - k_v[1])
split_points = [max(enumerate(xs[:-2]), key=dx(xs)) for xs in data["res"]]
quantil = float(sys.arg[2]) if len(sys.argv) > 2 else 0.5
split_value = sum([xs[int(quantil * len(xs))] for xs in data["res"]]) / rounds

fig, (ax_l, ax_h) = plt.subplots(1, 2, sharey=True)
fig.subplots_adjust(wspace=0.0)
ax_l.spines.right.set_visible(False)

ax_l.set_xlim(min([xs[0] for xs in data["res"]]), split_value)
ax_h.set_xlim(split_value, max([xs[-1] for xs in data["res"]]))

for i in range(rounds):
    xs = data["res"][i]
    split_index = list(filter(lambda k_v: k_v[1] > split_value, enumerate(xs)))[0][0]

    ax_l.plot(xs[:split_index], range(split_index), label=str(i + 1))
    ax_h.plot(xs[split_index:], range(split_index, len(xs)), label=str(i + 1))

plt.legend()

fig.add_subplot(111, frameon=False)
plt.tick_params(labelcolor='none', top=False, bottom=False, left=False, right=False)
plt.grid(False)
plt.xlabel("ratio to previous best")
plt.ylabel("refiner calls")

ax_l.axvline(1.0, linestyle=':', color='k')
ax_l.axhline(0.0, linestyle=':', color='k')

plt.show()

print(data["fails"])
