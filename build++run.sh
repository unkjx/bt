#!/bin/bash

if [[ "$*" == *"--a"* ]]; then
    $0 --d &
    if [[ "$*" == *"--x"* ]]; then
        $0 --x
    else
        $0
    fi
    wait
    exit 0
fi

CORES=4

# export SLEPC_DIR=/home/julian/Dokumente/Studium/BA/slepc-3.20.0-blopex/
# export SLEPC_DIR=/home/julian/Dokumente/Studium/BA/slepc-3.20.0-vanilla/
export SLEPC_DIR=/home/julian/Dokumente/Studium/BA/slepc-3.20.0/
export PETSC_DIR=/home/julian/Dokumente/Studium/BA/petsc-3.20.1/
# export PETSC_ARCH=arch-linux-c-debug
# export PETSC_ARCH=arch-linux-no-mpi
export PETSC_ARCH=linux-arch-no-mpi-blopex

if [[ "$*" == *"--d"* ]]; then
    TARGET_TYPE=Debug
    BUILD_DIR=build
else
    TARGET_TYPE=Release
    BUILD_DIR=build2
fi

if [[ "$*" == *"--b"* ]] || [ ! -d $BUILD_DIR ]; then
    mkdir -p $BUILD_DIR
    cd $BUILD_DIR
    DOWNLOAD_TBB_OPTION=OFF
    if [ ! -d external_tools/tbb/include ]; then
        DOWNLOAD_TBB_OPTION=ON
    fi
    cmake ../ -DCMAKE_BUILD_TYPE=$TARGET_TYPE -DKAHYPAR_ENABLE_THREAD_PINNING=OFF -DKAHYPAR_DOWNLOAD_TBB=$DOWNLOAD_TBB_OPTION
    cd ..
fi

# SHIPPED_INSTANCE="delaunay_n10.graph" # graph format!!!
# SHIPPED_INSTANCE="graph_with_edge_weights.graph" # graph format!!!
# SHIPPED_INSTANCE="twocenters.hgr" # bad cut (1.5)
# SHIPPED_INSTANCE="unweighted_hypergraph.hgr" # bad cut (1.5)
SHIPPED_INSTANCE="test_instance.hgr" # bad cut (1.5)
# SHIPPED_INSTANCE="hypergraph_with_edge_weights.hgr" # bad cut (2.5)
# SHIPPED_INSTANCE="delaunay_n15.graph.hgr" # works roughly
# SHIPPED_INSTANCE="sat14_atco_enc1_opt2_10_16.cnf.primal.hgr" # diverges, then breaks LAPACK
# SHIPPED_INSTANCE="contracted_ibm01.hgr" # bad cut (2)
# SHIPPED_INSTANCE="ibm01.hgr" # bad cut (2)
# SHIPPED_INSTANCE="karate_club.graph.hgr" # bad cut (1.5)
# SHIPPED_INSTANCE="powersim.mtx.hgr" # bad cut (5)
INSTANCE=../tests/instances/$SHIPPED_INSTANCE

# BM_INSTANCE=c/2cubes_sphere.mtx.hgr # works
# BM_INSTANCE=f/hypergraphs/amazon-2008.graph.hgr # diverges
# BM_INSTANCE=f/hypergraphs/as-22july06.graph.hgr # bad cut (1.5-2)
# BM_INSTANCE=f/hypergraphs/citationCiteseer.graph.hgr # diverges, then breaks LAPACK, then converges with bad cut (4)
# BM_INSTANCE=f/hypergraphs/cnr-2000.graph.hgr # diverges
# BM_INSTANCE=f/hypergraphs/PGPgiantcompo.graph.hgr # bad cut (1.5), then diverges, then breaks LAPACK, then converges with bad cut (3)
# BM_INSTANCE=g/hypergraphs/auto.graph.hgr # works roughly (1.1-3)
# BM_INSTANCE=g/hypergraphs/G3_circuit.graph.hgr # works
# BM_INSTANCE=g/hypergraphs/hugebubbles-00010.graph.hgr
# INSTANCE=../../test/10.35097-1196/data/dataset/benchmark_sets/benchmark_set_$BM_INSTANCE


if [[ "$*" == *"--ox"* ]] || ( cmake --build $BUILD_DIR --parallel $CORES --target MtKaHyPar && [[ "$*" == *"--x"* ]] ); then
   cd $BUILD_DIR
   export JULIA_NUM_THREADS=$CORES
   ./mt-kahypar/application/MtKaHyPar -h"$INSTANCE" -k2 -e0.03 -ocut -mdirect -t$CORES --preset-type=default -v1 --r-spectral-type=spectral --show-detailed-timings=1
   #--instance-type=graph --input-file-format=metis 
fi
