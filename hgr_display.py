import sys

import scipy as sp
from scipy.linalg import eigh

import numpy as np
import matplotlib.pyplot as plt

#from networkx.drawing.nx_agraph import graphviz_layout as layout

import hypernetx as hnx


def print_row(row, f="%.2f"):
    print("\t".join([f % v for v in row]))



# read file

f = open(sys.argv[1])

edges_incoming = False
edges_raw = {}
for line in f:
    line_str = line.strip()

    # skip comments
    if line_str.startswith("%"):
        continue

    # skip meta infos in first line
    if not edges_incoming:
        edges_incoming = True
        continue

    # add edge
    edges_raw[len(edges_raw)] = line_str.split(" ")

f.close()

print("read file")


# draw image and calculate wrong laplacian of hypergraph

H = hnx.Hypergraph(edges_raw)


# new graph, because numbering nodes disturbs layout somehow
edges = [[int(v) for v in e] for e in edges_raw.values()]
H_ = hnx.Hypergraph(edges)
print("created hg objects")
#A = np.diag([len(H_.neighbors(v)) for v in H_.nodes]) - H_.adjacency_matrix()

#print(A)


# print kspecpart paper matrix and calculate eigenpairs

n = len(H_.nodes)

def op_fun(x):
    res = np.zeros(n)
    for e in edges:
        w = 1./(-1.+len(e))
        one_e = np.array([1. if v in e else 0. for v in range(1, n+1)])
        res += w * (x - (np.dot(x, one_e) / np.dot(one_e, one_e)) * one_e)
    return res
#
def op_fun_j(x):
    res = np.zeros(n)
    for e in edges:
        w = 1./(-1.+len(e))
        one_e = np.array([1. if v in e else 0. for v in range(1, n+1)])
        res += w * (len(e) * (one_e * x) - np.dot(one_e, x) * one_e)
    return res

#op = lambda x : np.sum([1./(-1.+len(e)) * np.array([xj - np.sum([xi for i, xi in enumerate(x) if i+1 in e]) / len(e) for xj in x]) for e in edges], axis=0)
#

A_paper = [op_fun(ei) for ei in np.identity(n)]
A = [op_fun_j(ei) for ei in np.identity(n)]

for i in range(n):
    print_row(A_paper[i], "%+.2f")
print()

for i in range(n):
    print_row(A[i], "%+.2f")
print()

# eigenpairs
#eigvals, eigvecs = eigh(A)
#
#results = [op_fun_j(v) for v in eigvecs]
#
#
#print_row(eigvals, "%.2f:")
#for i in range(len(eigvals)):
#    print_row([v[i] for v in eigvecs])
#print_row(["--" for k in eigvals], "%s")
#for i in range(len(eigvals)):
#    print_row([r[i] - k * v[i] for r, k, v in zip(results, eigvals, eigvecs)])
#print()

while 1:
    hnx.drawing.draw(H)
    print("drawn hg")
    plt.show()
    if input():
        break
